const express = require("express");
const User = require("../models/sequelize/User");
const router = express.Router();

// GET ALL USERS
router.get("/", (req, res) => {
  if (!req.user || (!req.user.isAdmin()))
  {
    return res.sendStatus(403)
  };
  User.findAll({
    paranoid: false,
  })
    .then((data) => res.status(200).send(data))
    .catch((err) => res.sendStatus(500));
});

// GET A USER
router.get("/:id", (req, res) => {
  return User.findByPk(req.params.id).then((user) => {
    if (!user) {
      return res.sendStatus(404)
    }
    if (!req.user || (!req.user.isAdmin() && !user.isOwner(req.user))) {
      return res.sendStatus(403)
    }
    console.log(user);
    return res.json(user);
  }).catch(err => {
    console.error(err);
    return res.sendStatus(500);
  });
});

// PUT A USER
router.put("/:id/update", async (req, res) => {
  if (!req.user.isAdmin()  && req.user.id !== req.params.id) return res.sendStatus(403);
  User.update(req.body, {returning: true, where: { id: req.params.id } })
    .then((result) => res.json(result)
     )
    .catch((err) => {
      console.error(err);
      return res.sendStatus(500);
    });
});

// DELETE A USER
router.delete("/:id/delete", (req, res) => {
  if (!req.user.isAdmin()) return res.sendStatus(403);
  User.destroy({
    where: { id: req.params.id },
  })
    .then(data => {
      if(data){
        res.sendStatus(204)
      }else{
        res.sendStatus(404)
      }
    })
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
