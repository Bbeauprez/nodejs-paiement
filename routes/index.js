const SecurityRouter = require("./security");
const TransactionRouter = require("./transactions");
const userRouter = require("./users");
const merchantRouter = require('./merchants')
//const DashboardRouter = require("./dashboard");
const verifyToken = require("../middlewares/verifyToken");
const verifyMerchant = require("../middlewares/merchantAuthentication");
const cors = require('cors');

const routerManager = (app) => {
  app.use(cors());
  app.use("/", SecurityRouter);
  app.use(verifyToken);
  app.use("/users", userRouter);
  app.use("/merchants", merchantRouter)
  //app.use('/dashboard',DashboardRouter);
  app.use(verifyMerchant);
  app.use('/transactions', TransactionRouter);
};

module.exports = routerManager;
