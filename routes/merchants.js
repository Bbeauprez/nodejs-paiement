const express = require("express");
const {Merchant, Address, Transaction, Op} = require("../models/sequelize");
const router = express.Router();

//Create a merchant
router.post('/', (req, res) => {
    const data = {...req.body};
    data.status = 'pending';
    data.UserId = req.user.id;

    return Merchant.create(data)
        .then(merchant => {
            const merchantObj = merchant.toJSON();
            res.status(201).json(merchantObj);
        })
        .catch(err => {
            console.error(err)
        });
});

// GET ALL MERCHANTS
router.get('/', (req, res) => {
    Merchant.findAll({where: {UserId: req.user.id}})
        .then(merchants => res.json(merchants))
        .catch(err => {
            console.error(err);
            return res.sendStatus(500);
        });
})

// GET A MERCHANT
router.get("/:id", (req, res) => {
    return Merchant.findByPk(req.params.id).then(merchant => {
        if (!merchant) {
            return res.sendStatus(404)
        }
        if (!req.user || (!req.user.isAdmin() && !merchant.isOwner(req.user))) {
            return res.sendStatus(403)
        }
        console.log(merchant);
        return res.json(merchant);
    }).catch(err => {
        console.error(err);
        return res.sendStatus(500);
    });
});

// PUT A MERCHANT
router.put("/:id/activate", async (req, res) => {
    Merchant.findByPk(req.params.id).then(merchant => {
        if (!merchant) {
            return res.sendStatus(404)
        }
        if (!req.user || !req.user.isAdmin()) {
            return res.sendStatus(403)
        }
        merchant.status = 'validated';
        merchant.addCredentials()
            .then(merchant => res.status(200).send(merchant))
            .catch(err => {
                console.error(err);
                res.sendStatus(500);
            })
    }).catch(err => {
        console.error(err);
        return res.sendStatus(500);
    });


})

router.get("/:id/credentials", async (req, res) => {
    Merchant.findByPk(req.params.id).then(merchant => {
        if (!merchant) {
            return res.sendStatus(404)
        }
        if (!req.user || (!req.user.isAdmin() && !merchant.isOwner(req.user))) {
            return res.sendStatus(403)
        }

        merchant.addCredentials().then(merchant => res.json({token: merchant.token, secret: merchant.secret}))
            .catch(err => {
                console.error(err);
                res.sendStatus(500);
            })
    });
})

router.delete("/:id/deactivate", async (req, res) => {
    Merchant.findByPk(req.params.id).then(merchant => {
        if (!merchant) {
            return res.sendStatus(404)
        }
        if (!req.user || (!req.user.isAdmin() && !merchant.isOwner(req.user))) {
            return res.sendStatus(403)
        }
        merchant.token = null;
        merchant.secret = null;
        merchant.save()
            .then(merchant => res.json(merchant))
            .catch(err => {
                console.error(err);
                res.sendStatus(500);
            })
    });
})

// router.get("/:id/transactions", async (req, res) => {
// 	const merchant = await adminMerchant(req,res);

// 	return Transaction.findAll({
// 		where: {MerchantId: merchant.id},
// 		include: [{
// 			model: Operation
// 		}, {
// 			model: Address,
// 			as: 'billing'
// 		}, {
// 			model: Address,
// 			as: 'shipping'
// 		}]
// 	})
// 		.then(transactions => res.json(transactions))
// 		.catch(err => {
// 			console.error(err);
// 			return res.sendStatus(500);
// 		});
// });
module.exports = router;
