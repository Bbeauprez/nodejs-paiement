const mongoose = require("mongoose");
const {Schema} = mongoose;

const Merchantschema = new Schema(
    {
        society_name: {type: String},
        KBIS: {type: String},
        contactInfos: {type: String},
        confirmedUrl: {type: String},
        cancelledUrl: {type: String},
        currency: {type: String},
        email: {type: String},
        status: {type: String},
        merchant_token: {type: String},
        merchant_secret: {type: String},
        createdAt: {type: Date},
        updatedAt: {type: Date},
        deletedAt: {type: Date}
    }
);

module.exports = mongoose.model("Merchant", Merchantschema);


