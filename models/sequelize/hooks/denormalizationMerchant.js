const MerchantMongo = require("../../mongoose/Merchant");

const denormalizeMerchant = async (ModelPG, merchantID, operation) => {
	await MerchantMongo.deleteOne({ id: merchantID });

	if (operation !== "delete") {
		// Get User with association in DB if not delete
		const dbMerchant = await ModelPG.findOne({
			where: { id: merchantID },
			include: [
				{all: true}
			],
		});

		// Save in mongo
		const mMerchant = new MerchantMongo(dbMerchant.toJSON());
		await mMerchant.save();
	}
};

module.exports = denormalizeMerchant;
