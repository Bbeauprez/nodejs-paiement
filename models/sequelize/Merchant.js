const sequelize = require("../../lib/sequelize");
const {DataTypes, Model} = require("sequelize");
const User = require("./User");
const denormalize = require("./hooks/denormalizationMerchant");
const {v4: uuidv4} = require('uuid');


class Merchant extends Model {
    isOwner(user) {
        return this.UserId === user.id;
    }

    addCredentials() {
        this.token = uuidv4();
        this.secret = Math.random().toString(36).slice(-8);
        return this.save();
    }

    extractCredentials(header) {
        const [token, secret] = Buffer.from(header, 'base64').toString('ascii').split(':');
        return {token, secret};
    }
}

Merchant.init(
    {
        society_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        KBIS: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        phoneNumber: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        confirmedUrl: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        cancelledUrl: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        currency: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        token: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        secret: {
            type: DataTypes.STRING,
            allowNull: true,
        }

    },
    {
        sequelize,
        modelName: "Merchant",
        paranoid: true,
    }
);


Merchant.belongsTo(User); // Merchant.user
User.hasMany(Merchant); // Users.merchant

Merchant.addHook("afterCreate", (merchant) => {
    denormalize(Merchant, merchant.id, "create").then(() => {
    }).catch(console.error);
});
Merchant.addHook("afterUpdate", (merchant) => {
    denormalize(Merchant, merchant.id, "update");
});
Merchant.addHook("afterDestroy", (merchant) => {
    denormalize(Merchant, merchant.id, "delete");
});

module.exports = Merchant;
