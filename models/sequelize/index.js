const sequelize = require("../../lib/sequelize");
const Merchant = require('./Merchant');
const User = require('./User');
const Transaction = require('./Transaction');
const Address = require('./Address');
const Operation = require('./Operation');

// require("./hooks");

sequelize
	.sync({ force: true })
	.then((result) => console.log("Sequelize models synced"))
	.catch((error) => console.error("Error while syncing models", error));

module.exports = {
	sequelize,
	User,
	Merchant,
	Transaction,
	Operation,
	Address
};
