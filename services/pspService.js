const axios = require('axios');

class PspService {
  static processPayment(transactionId, amount, CCData) {
    return axios.get('http://psp:3000',
      {
        data: {
          transactionId,
          amount,
          creditCardNumber: CCData.CCNumber,
          creditCardOwner: CCData.CCOwner,
          expirationDate: CCData.CCExpirationDate,
          secureCode: CCData.CCSecureCode
        }
      }
    ).then(()=>{}).catch(console.error);
  }
}

module.exports = PspService
